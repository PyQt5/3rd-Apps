# 3rd-Apps
Collecting 3rd Apps

- 目录
  - [一些Qt写的三方APP](https://github.com/892768447/PyQt/wiki/3rd-party-applications)
  - [kdPythonAPIViewer【通过GUI界面查看python或第三方库的API】](https://github.com/bkdwei/kdPythonAPIViewer)
  - [JQTools【基于Qt开发的小工具包】](https://github.com/188080501/JQTools)
  - [AppiumProjects【基于Appium框架开发的自动化程序】](https://github.com/codingZXY/AppiumProjects)
  - [Pyqt写的git可视化工具](https://github.com/git-cola/git-cola)
  - [一款PyQt音乐播放器](https://github.com/ffwff/aidoru/tree/master)
  - [Qt使GTK主题QGtkStyleNoneX11](https://github.com/yennar/QGtkStyleNoneX11)
  - [属性编辑器1](https://github.com/LeftRadio/PyQtPropertyBrowser)
  - [属性编辑器2](https://github.com/theall/QtPropertyBrowserV2.6-for-pyqt5)
